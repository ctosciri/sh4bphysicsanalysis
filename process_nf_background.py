import uproot
import numpy as np

from sig_region_2d_fit import apply_2d_cut

years = [2016, 2017, 2018]
# default_xbins = np.logspace(np.log10(4e2), np.log10(7e3), 201)
# default_xbins = np.linspace(4e2, 7e3, 201)

s_masses = [70, 100, 170, 200, 250, 300, 400, 500, 750, 1000, 1500, 2000, 2500, 3000, 4000, 5000]
x_masses = [300, 400, 750, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 6000]

s_mass_bounds = \
{
    70: (51.016492966332834, 86.10361152805339),
    100: (41.93689704145831, 144.35017581978818),
    170: (117.44606353123945, 207.54474446805628),
    200: (136.59141836353228, 237.00081501252282),
    250: (168.73831889142787, 291.65261571049314),
    300: (201.99719986383417, 348.2991825678708),
    400: (267.6219944794021, 458.1254853548739),
    500: (336.08535875740887, 565.1127691201983),
    750: (509.4051967753908, 830.3135969236234),
    1000: (683.0826778437853, 1101.1336318837016),
    1500: (1006.9508899816418, 1639.7853840351195),
    2000: (1308.886662166531, 2200.1970730120374),
    2500: (1712.0512286740889, 2697.4648775331734),
    3000: (1989.1118038710538, 3270.6058611941608),
    4000: (2615.6022694276903, 4359.170135161537),
    5000: (3461.3982048188245, 5434.148975902206)
}

def get_background_file_name(year, trigger_tag, model_tag, btag, xbbtag):
    if model_tag == "unrestricted":
        return "data" + str(year - 2000) + "_flows_" + trigger_tag + f"btag{btag}_xbbtag{xbbtag}.root"
    else:
        return "data" + str(year - 2000) + "_flows_" + trigger_tag + "_" + model_tag + f"btag{btag}_xbbtag{xbbtag}.root"


def get_background_tree_path(directory, year, trigger_tag, model_tag, btag, xbbtag):
    '''
    get path to tree in root file for uproot
    '''
    file_name = get_background_file_name(year, trigger_tag, model_tag, btag, xbbtag)
    return directory + "/" + file_name + ":trees/mixed__signal"


def get_x_masses_from_tree(year, s_mass, x_mass, seed, file, settings):
    '''
    read in x masses from tree as pandas dataframe
    '''
    # selection_string = get_background_selection_string(s_mass, x_mass, seed)
    branches = file.arrays(["X_mass", "S_mass", "seed"], library="pd")
    branches = apply_selection(branches, s_mass, x_mass, settings)
    branches = select_seed(branches, seed)
    branches.reset_index(drop=True, inplace=True) # so indices are continuous, need this for root input
    return branches


def apply_selection(branches, s_mass, x_mass, settings):
    '''
    Applies the signal region selection
    '''
    
    if settings['sig_region'] == '1dfit':
        s_mass_cut = s_mass_cut_string(s_mass)
        branches = branches.query(s_mass_cut)
    elif settings['sig_region'] == '2dfit':
        x = branches['X_mass'].values
        s = branches['S_mass'].values
        _, _, mask = apply_2d_cut(x_mass, s_mass, x, s)
        branches = branches[mask]
    elif settings['sig_region'] == 'None':
        branches = branches
    else:
        print('Need to specify type of selection')
        exit(0)
    
    return branches
    
def select_seed(branches, seed):
    '''
    Selects one seed from the inpurt TTree
    '''
    seed_requirement = "(seed == " + str(seed) + ")"
    return branches.query(seed_requirement)
    
# def get_background_selection_string(s_mass, x_mass, seed):
#     '''
#     only get data in the s mass range from a given seed
#     we don't need to select for the trigger or region because it is baked into the model
#     '''
    
#     s_mass_cut = s_mass_cut_string(s_mass)
#     seed_requirement = "(seed == " + str(seed) + ")"
#     return s_mass_cut + " & " + seed_requirement


def s_mass_cut_string(nominal_s_mass):
    min_s_mass = s_mass_bounds[nominal_s_mass][0]
    max_s_mass = s_mass_bounds[nominal_s_mass][1]
    return "(S_mass > {:.3f}) & (S_mass < {:.3f})".format(min_s_mass, max_s_mass)



def histogram_x_masses(branches, bins, weight=None):
    '''
    histogram the x masses from the tree
    '''
    n_values = len(branches)
    weight = None
        
    values = branches["X_mass"].values

    if weight != None:
        weights = branches[weight].values
    else:
        weights = np.ones(n_values)

    h, bins = np.histogram(values, bins=bins, weights=weights)
    h_w2, _ = np.histogram(values, bins=bins, weights=weights**2)

    histogram = (
        h,            # the bin content
        bins,         # the bins
        np.sqrt(h_w2) # the uncertainty per bin
    )

    return histogram


    
# def get_sr_scale_factors(directory, trigger_tag, model_tag, n_seeds):
def get_sr_scale_factors(settings):
    '''
    return number of expected inclusive SR events for each year
    also store total number of SR events for each seed
    '''
    norms = {}
    totals = {year: {} for year in years}

    # get norm from histogram
    for year in years:
        file_name = get_background_file_name(year, settings['trigger_tag'], settings['model_tag'], settings['btag'], settings['xbbtag'])
        norm_histogram_path = settings['directory'] + "/" + file_name + ":cutflows/norm_SR"
        with uproot.open(norm_histogram_path) as file:
            norms[year] = file.values()[0]

        # get event count for each seed
        tree_path = get_background_tree_path(settings['directory'],
                                             year, settings['trigger_tag'], settings['model_tag'],
                                             settings['btag'], settings['xbbtag'])
        with uproot.open(tree_path) as file:
            all_events = file.arrays(["seed"], library="pd")
        for seed in range(settings['n_seeds']):
            mask = (all_events["seed"] == seed)
            totals[year][seed] = len(all_events[mask])

    # return scale factors
    return {year: {seed: norms[year] / totals[year][seed] for seed in range(settings['n_seeds'])} for year in years}


# def get_all_background_histograms(directory, trigger_tag, model_tag, n_seeds):
def get_all_background_histograms(settings):
    '''
    create x mass histograms for all years and all s masses
    '''
    # first retrieve scale factors for normalization
    scale_factors = get_sr_scale_factors(settings)

    histograms = {s_mass: {x_mass: {seed: {} for seed in range(settings['n_seeds'])} for x_mass in x_masses} for s_mass in s_masses}
    for year in years:
        print(f'Getting backgrounds for year = {year}')
        tree_path = get_background_tree_path(settings['directory'],
                                             year, settings['trigger_tag'], settings['model_tag'],
                                             settings['btag'], settings['xbbtag'])
        with uproot.open(tree_path) as file:
            for s_mass in s_masses:
                for x_mass in x_masses:
                    for seed in range(settings['n_seeds']):
                        branches = get_x_masses_from_tree(year, s_mass, x_mass, seed, file, settings)
                        histograms[s_mass][x_mass][seed][year] = histogram_x_masses(branches, bins=settings['bins'])
                        histograms[s_mass][x_mass][seed][year] = \
                            (histograms[s_mass][x_mass][seed][year][0] * scale_factors[year][seed],
                             histograms[s_mass][x_mass][seed][year][1],
                             histograms[s_mass][x_mass][seed][year][2] * scale_factors[year][seed])
    return histograms

# def get_all_background_histograms_unscaled(settings):
#     '''
#     create x mass histograms for all years and all s masses
#     '''
#     # first retrieve scale factors for normalization
#     scale_factors = get_sr_scale_factors(settings)

#     histograms = {s_mass: {x_mass: {seed: {} for seed in range(settings['n_seeds'])} for x_mass in x_masses} for s_mass in s_masses}
#     for year in years:
#         print(f'Getting backgrounds for year = {year}')
#         tree_path = get_background_tree_path(settings['directory'],
#                                              year, settings['trigger_tag'], settings['model_tag'],
#                                              settings['btag'], settings['xbbtag'])
#         with uproot.open(tree_path) as file:
#             for s_mass in s_masses:
#                 for x_mass in x_masses:
#                     for seed in range(settings['n_seeds']):
#                         branches = get_x_masses_from_tree(year, s_mass, x_mass, seed, file, settings)
#                         histograms[s_mass][x_mass][seed][year] = histogram_x_masses(branches)
                        
#     return histograms, scale_factors

def sum_histograms(hist1, hist2):
    h = hist1[0] + hist2[0]
    b = hist1[2]

    err1 = hist1[2]
    err2 = hist2[2]
    err = np.sqrt(err1**2 + err2**2)

    return (h, b, err)

def get_summed_background_histograms(all_histograms, n_seeds):
    '''
    sum x mass histograms over all three years
    '''
    histograms = {s_mass: {x_mass: {} for x_mass in x_masses} for s_mass in s_masses}
    for s_mass in s_masses:
        for x_mass in x_masses:
            for seed in range(n_seeds):
                histogram_name = "background_" + str(s_mass) + "_" + str(seed)
                histograms[s_mass][x_mass][seed] = all_histograms[s_mass][x_mass][seed][years[0]]
                for i in range(1, len(years)):
                    histograms[s_mass][x_mass][seed] = sum_histograms(histograms[s_mass][x_mass][seed],
                                                                      all_histograms[s_mass][x_mass][seed][years[i]])
                    # (histograms[s_mass][x_mass][seed][0] + all_histograms[s_mass][x_mass][seed][years[i]][0],
                    #                             histograms[s_mass][x_mass][seed][1])
    return histograms



def normalize_histograms_to_unity(histograms, n_seeds):
    '''
    normalize variation histograms to unity before computing the covariance matrix
    histograms are stored in a dictionary with seed number as key
    have to give them new names so root doesn't compain
    '''
    normalized_histograms = {}
    for seed in range(n_seeds):
        # histogram_name = histograms[seed].GetName() + "_norm"
        # integral = histograms[seed].Integral()
        bins = histograms[seed][1]
        integral = np.sum(histograms[seed][0])
        if integral > 0:
            # normalized_histograms[seed] = histograms[seed].Clone(histogram_name)
            # normalized_histograms[seed].Scale(1 / integral)
            normalized_histograms[seed] = (histograms[seed][0] / integral, histograms[seed][1])
        else: # fill empty histograms with a small value to avoid calculation issues
            bins_ = np.full_like(bins, 1e-10)
            # normalized_histograms[seed] = make_histogram_from_bins(histogram_name, bins)
            normalized_histograms[seed] = (bins_, bins)
    return normalized_histograms


def get_covariance_matrix(histograms, n_seeds):
    '''
    get covariance matrix for a single s mass
    histograms are stored in a dictionary with seed number as key
    '''
    normalized_histograms = normalize_histograms_to_unity(histograms, n_seeds)
    histogram_stack = np.array([normalized_histograms[seed][0] for seed in range(n_seeds)])
    covariance_matrix = np.cov(histogram_stack, rowvar=False)

    # make sure matrix is positive definite, and remove weird values from empty bins
    covariance_matrix[(covariance_matrix < 0) | (covariance_matrix > 1)] = 0

    return covariance_matrix


def get_covariance_matrices(histograms, n_seeds):
    '''
    return a dict of covariance matrics for all s masses
    '''
    covariance_matrices = {}
    for s_mass in s_masses:
        covariance_matrices[s_mass] = {}
        for x_mass in x_masses:
            covariance_matrices[s_mass][x_mass] = get_covariance_matrix(histograms[s_mass][x_mass], n_seeds)
            
    return covariance_matrices



def get_average_background_histogram(histograms, n_seeds):
    '''
    get the "nominal" background distribution for a given s mass by computing the average of the 25 variations
    '''
    average_histogram = histograms[0]
    for seed in range(1, n_seeds):
        average_histogram = sum_histograms(average_histogram,  histograms[seed])
        # (average_histogram[0] + histograms[seed][0], average_histogram[1])
    average_histogram = (average_histogram[0] / n_seeds, average_histogram[1], average_histogram[2] / n_seeds)
    return average_histogram


def get_average_background_histograms(histograms, n_seeds):
    '''
    get the nominal background histogram for all s masses
    '''
    return {s_mass: {x_mass: get_average_background_histogram(histograms[s_mass][x_mass], n_seeds) for x_mass in x_masses} for s_mass in s_masses}



def get_background_variations(nominal_background, covariance_matrix):
    '''
    create up and down variations for the nominal background using pca (i.e. eigenvalues/vectors)
    '''
    # nominal_histogram_name = nominal_background.GetName()
    # background_yield = nominal_background.Integral()
    background_yield = np.sum(nominal_background[0])
    bins = nominal_background[1]
    
    # do the pca part
    eigenvalues, eigenvectors = np.linalg.eigh(covariance_matrix)
    eigenvalues[eigenvalues < 0] = 0 # again account for poor conditioning
    perturbations = np.dot(eigenvectors, np.sqrt(eigenvalues))

    # turn this into a root histogram
    # perturbation_histogram = make_histogram_from_bins(nominal_histogram_name + "_bins", perturbations)
    # perturbation_histogram.Scale(background_yield) # account for normalization
    perturbation_histogram = (perturbations * background_yield, bins)
    
    # get variations
    # up_variation = nominal_background.Clone(nominal_histogram_name + "_up")
    # up_variation.Add(perturbation_histogram)
    # down_variation = nominal_background.Clone(nominal_histogram_name + "_down")
    # down_variation.Add(-1 * perturbation_histogram)
    up_variation = (nominal_background[0] + perturbation_histogram[0], bins)
    down_variation = (nominal_background[0] - perturbation_histogram[0], bins)

    return up_variation, down_variation


def get_all_background_variations(average_histograms, covariance_matrices):
    '''
    create variation histograms for all s masses
    '''
    up_variations, down_variations = {}, {}
    for s_mass in s_masses:
        up_variations[s_mass] = {}
        down_variations[s_mass] = {}
        for x_mass in x_masses:
            up_variations[s_mass][x_mass], down_variations[s_mass][x_mass] = get_background_variations(average_histograms[s_mass][x_mass], covariance_matrices[s_mass][x_mass])
    return up_variations, down_variations


def process_background(settings):
    
    if settings['model_tag'] == "":
        settings['model_tag'] = "unrestricted"

    # create background histograms
    all_histograms = get_all_background_histograms(settings)
    summed_histograms = get_summed_background_histograms(all_histograms, settings['n_seeds'])
    
    # all_unscaled_histograms = get_all_background_unscaled_histograms(settings)
    # summed_unscaled_histograms = get_summed_background_histograms(all_unscaled_histograms, settings['n_seeds'])
    
    # get covariance matrices for fit uncertainty
    covariance_matrices = get_covariance_matrices(summed_histograms, settings['n_seeds'])

    # find average of n_seeds variations for "nominal" background distributions
    average_histograms = get_average_background_histograms(summed_histograms, settings['n_seeds'])
    # average_unscaled_histograms = get_average_background_histograms(summed_unscaled_histograms, settings['n_seeds'])
    # then get variations
    up_variations, down_variations = get_all_background_variations(average_histograms, covariance_matrices)
    
    bkg_histograms = {}
    bkg_histograms_up = {}
    bkg_histograms_dw = {}
    # bkg_unscaled_histograms = {}
    
    for S in s_masses:
        for X in x_masses:
            bkg_histograms[f"{X}_{S}"] = average_histograms[S][X]
            bkg_histograms_up[f"{X}_{S}"] = up_variations[S][X]
            bkg_histograms_dw[f"{X}_{S}"] = down_variations[S][X]
            # bkg_unscaled_histograms[f"{X}_{S}"] = average_unscaled_histograms[S][X]
    
    return bkg_histograms, bkg_histograms_up, bkg_histograms_dw#, bkg_unscaled_histograms
