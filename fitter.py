import os

import numpy as np
import matplotlib.pyplot as plt

import json
import pyhf
pyhf.set_backend('numpy')
from pyhf.contrib.viz import brazil

prop_cycle = plt.rcParams['axes.prop_cycle']
_colors_ = prop_cycle.by_key()['color']

class Fitter:
    
    def __init__(self, signal, background, bins, X, S, debug=True, save_dir='./plots_fit/'):
        
        self._signal = signal
        self._background = background

        self._bkg_var_hi = None
        self._bkg_var_low = None
        self._bkg_mcstat_unc = None
        
        self._bins = bins
        self._X = X
        self._S = S
        self._debug = debug
        self._save_dir = save_dir
        
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)


    def set_bkg_vars(self, bkg_var_hi, bkg_var_low):
        '''
        Sets the background up and down variations
        '''
        self._bkg_var_hi = bkg_var_hi
        self._bkg_var_low = bkg_var_low

    def set_bkg_mcstat_unc(self, bkg_mcstat):
        '''
        Sets the background MC uncertainties
        '''
        self._bkg_mcstat_unc = bkg_mcstat
        
        
    def get_workspace(self):
        
        # print(self._background - self._bkg_var)
        
        # bkg_var_hi = (self._bkg_var - self._background).tolist()
        # bkg_var_lo = (self._background - self._bkg_var).tolist()
        
        # print(bkg_var_hi / self._background * 100)
        
        # bkg_var_hi = [0.1] * len(self._background)
        # bkg_var_lo = [0.1] * len(self._background)
        
        workspace_def = {
            "channels": [
                { "name": "singlechannel",
                  "samples": [
                      { "name": "signal",
                        "data": self._signal.tolist(),
                        "modifiers": [
                            {"name": "mu", "type": "normfactor", "data": None}
                        ],
                      },
                      { "name": "background",
                        "data": self._background,
                        "modifiers": [
                            # { "name": "uncorr_bkguncrt", "type": "shapesys", "data": np.sqrt(self._background).tolist(), }
                            # { "name": "uncorr_bkguncrt", "type": "shapesys", "data": (0.10 * self._background).tolist(), }
                            # { "name": "normsys", "type": "normsys", "data": {"hi": 0.9, "lo": 1.1}, }
                            { 
                                "name": "corr_bkguncrt",
                                "type": "histosys",
                                "data": {"hi_data": self._bkg_var_hi.tolist(), "lo_data": self._bkg_var_low.tolist()}
                            },
                            {
                                "name": "bkg_mcstaterror",
                                "type": "staterror",
                                "data": self._bkg_mcstat_unc.tolist(),
                            }
                        ],
                      }
                   ]
                }
            ],
            "measurements": [
                { "config": {"parameters": [{"bounds": [[0, 350]], "inits": [1.0], "name": "mu"}],
                             "poi": "mu" },
                  "name": "cat_limits"
                }
            ],
            "observations": [
                { "name": "singlechannel",
                  "data": self._background.tolist()},
            ],
            "version": "1.0.0"
        }
        
        return workspace_def
    
    def prepare_fit(self, plot_summary=False):

        self._workspace = pyhf.Workspace(self.get_workspace())
        
        model = self._workspace.model()
        
        if (self._debug):
            print(f"  channels: {model.config.channels}")
            print(f"     nbins: {model.config.channel_nbins}")
            print(f"   samples: {model.config.samples}")
            print(f" modifiers: {model.config.modifiers}")
            print(f"parameters: {model.config.parameters}")
            print(f"  nauxdata: {model.config.nauxdata}")
            print(f"   auxdata: {model.config.auxdata}")
            
#             print(f"      down: {model.expected_data([-1.0, 1])}")
#             print(f"   nominal: {model.expected_data([0.0, 1])}")
#             print(f"        up: {model.expected_data([1.0, 1])}")
            
#             print(model.config.suggested_init(), model.config.poi_index)
#             print(model.config.parameters)
            

        if plot_summary:
            n_bins = model.config.channel_nbins['singlechannel']
            bins = np.linspace(0, n_bins+1, n_bins+1)

            init_pars = model.config.suggested_init()

            # Make a copy, only for plotting purposes
            bkg_pars = init_pars.copy()
            # print(bkg_pars)
            
            # Get the index of mu and the parameter of the bkg up and dw variation
            mu_index = model.config.poi_index
            # bkg_index = model.config.parameters.index('corr_bkguncrt')
            # bkg_mcstat_index = model.config.parameters.index('bkg_mcstaterror')

            par_order = model.config.par_order
            
            bkg_index = par_order.index('corr_bkguncrt')
            bkg_mcstat_index = par_order.index('bkg_mcstaterror')
            bkg_mcstat_npars = model.config.param_set('bkg_mcstaterror').n_parameters
            # bkg_mcstat_sigmas = model.config.param_set('bkg_mcstaterror').sigmas
            # print('mu_index', mu_index)
            # print('bkg_index', bkg_index)
            # print('bkg_mcstat_index', bkg_mcstat_index, bkg_mcstat_npars)
            # print('bkg_mcstat_sigmas', bkg_mcstat_sigmas)
            # print('self._bkg_mcstat_unc', self._bkg_mcstat_unc)

            # Set the signal to zero so we have background only
            bkg_pars[mu_index] = 0
            background = model.expected_actualdata(bkg_pars)
            
            # Set the signal back to one so we have background+signal
            bkg_pars[mu_index] = 1
            total = model.expected_actualdata(bkg_pars)
            signal = total - background
            
            # Get the up and down variations on the background
            bkg_pars[mu_index] = 0
            bkg_pars[bkg_index] = -1
            bkg_up = model.expected_actualdata(bkg_pars)
            bkg_pars[bkg_index] = +1
            bkg_dw = model.expected_actualdata(bkg_pars)

            # Get the background stat unc
            # bkg_pars[mu_index] = 0
            # bkg_pars[bkg_index] = 0
            # bkg_pars[bkg_mcstat_index:bkg_mcstat_index+bkg_mcstat_npars] = [1]*bkg_mcstat_npars
            bkg_mcstat_p = background + self._bkg_mcstat_unc
            bkg_mcstat_m = background - self._bkg_mcstat_unc
            
            data = self._workspace.data(model, include_auxdata=False)

            fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(5, 5))
            plt.stairs(background, bins, color=_colors_[0], label='Background')
            plt.stairs(signal, bins, color=_colors_[1], label='Signal')
            plt.stairs(total, bins, color=_colors_[2], label='Signal+Background')
            
            # Add bkg variations if available
            plt.stairs(bkg_up, bins, label='Bkg Up Variation',
                       color=_colors_[0], linestyle='dashed', alpha=0.8)
            plt.stairs(bkg_dw, bins, label='Bkg Down Variation',
                       color=_colors_[0], linestyle='dotted', alpha=0.8)
            
            # Add bkg mc stat uncertaintie
            plt.fill_between(bins,
                             np.append(bkg_mcstat_m, bkg_mcstat_m[-1]), 
                             np.append(bkg_mcstat_p, bkg_mcstat_p[-1]),
                             alpha=0.2, color=_colors_[0], step='post',
                             label='Bkg MC Stat Unc')
            
            # Add the data
            # plt.plot(bins[:-1]+0.5, data, 'o', color='black', markersize=3, label='Observation')

            ax.legend()
            ax.set_xlabel('Bins')
            ax.set_ylabel('Events')
            ax.set_title(f'X = {self._X} GeV, S = {self._S} GeV')
            plt.savefig(self._save_dir + f'/hists_X{self._X}_S{self._S}.pdf')
            # plt.show()
            plt.close()


    def fit(self, poi_values=None):
        
        if poi_values is None:
            poi_values = np.linspace(0.1, 5, 50)
        
        model = self._workspace.model()
        observations = self._workspace.data(model, include_auxdata=True)
        
        if (self._debug): print('Running fit...')
        obs_limit, exp_limits, (scan, results) = pyhf.infer.intervals.upper_limits.upper_limit(
            observations, model, poi_values, level=0.05, return_results=True
        )
        if (self._debug): print('... fit completed.')

        if (self._debug): print(f"Upper limit (obs): μ = {obs_limit:.4f}")
        if (self._debug): print(f"Upper limit (exp): μ = {exp_limits[2]:.4f}")
        
        return obs_limit, exp_limits[2]