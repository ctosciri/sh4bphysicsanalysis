import os
import glob
import uproot
import awkward as ak
import numpy as np
import matplotlib.pyplot as plt

# from from_lacey.make_fit_histograms import *
from sig_region_2d_fit import apply_2d_cut

prop_cycle = plt.rcParams['axes.prop_cycle']
_colors_ = prop_cycle.by_key()['color']


_default_xbins_ = np.logspace(np.log10(4e2), np.log10(7e3), 30)
# _default_xbins_ = np.linspace(4e2, 7e3, 30)
default_n_iterations = 25

def get_signal(signalDir = "/data/rainbolt/sh4b/mixed_selection/signal/",
               data="2018", mc20="mc20e", btag=77, xbbtag=70, debug=False):
    
    print('Getting signals from', signalDir)
    print('for', data, ',', mc20)

    if (data == "2018"):
        trigger_LRj = 'HLT_j420_a10t_lcw_jes_35smcINF_L1J100'
        trigger_2b1j = 'HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30'
        trigger_2b2j = 'HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25'
    elif (data == "2017"):
        trigger_LRj = 'HLT_j420_a10t_lcw_jes_40smcINF_L1J100'
        trigger_2b1j = 'HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30'
        trigger_2b2j = 'HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25'
    elif (data == "2016"):    
        trigger_LRj  = 'HLT_j420_a10_lcw_L1J100'
        trigger_2b1j = 'HLT_j100_2j55_bmv2c2060_split'
        trigger_2b2j = 'HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25'
            
    columns_to_load = [
        "X_mass",
        "S_mass",
        "H_mass",
        "leading_large_r_jet_pt",
        "S_jet1_pt",
        "beamspot_weight",
        trigger_LRj,
        trigger_2b1j,
        # trigger_2b2j
    ]

    truth_X = []
    truth_S = []

    signals = {}
    lumi_weights = {}
    evt_weights = {}
    sig_hists = {}
    
    filenames = glob.glob(signalDir + f'/{mc20}_*_btag{btag}_xbbtag{xbbtag}.root') # list

    for filename in filenames:
        
        if(debug): print('Looking at', filename)

        X = filename.split('mX')[1].split('_mS')[0]
        S = filename.split('mS')[1].split('_btag')[0]  
        X = int(X)
        S = int(S)

        df = None 

        tree = uproot.open(filename + ':trees')
        # sig_tree = tree['mixed_trig2b1j_signal']
        # sig = sig_tree.arrays(columns_to_load)#, library="pd")
        
        sig = {
            '2b1j': tree['mixed_trig2b1j_signal'].arrays(columns_to_load + ['pileup_weight_bjet']),
            'LR': tree['mixed_trigLR_signal'].arrays(columns_to_load + ['pileup_weight_nominal']),
        }
        sig['2b1j']['pileup_weight'] = sig['2b1j']['pileup_weight_bjet']
        sig['LR']['pileup_weight'] = sig['LR']['pileup_weight_nominal']
        
        # evt_weight = cutflow['sum_of_weights'].values()[0]
        
        
        # Add sum_of_weights to the tree
        cutflow = uproot.open(filename + ':cutflows')
        # evt_weight = {
        #     '2b1j': cutflow['sum_of_weights'].values()[1],
        #     'LR': cutflow['sum_of_weights'].values()[0]
        # }
        sig['2b1j']['sum_of_weights'] = [cutflow['sum_of_weights'].values()[1]] * len(sig['2b1j'])
        sig['LR']['sum_of_weights'] = [cutflow['sum_of_weights'].values()[0]] * len(sig['LR'])
        
        # Add lumi_weight to the tree
        lumi_weight = apply_weights(sig, data)
        sig['2b1j']['lumi_weight'] = lumi_weight['2b1j']
        sig['LR']['lumi_weight'] = lumi_weight['LR']
                
        sig = np.concatenate([sig['2b1j'], sig['LR']])
        
        
#         if (mc20 == "mc20e"):
#             LRj_trigger_mask = (sig['HLT_j420_a10t_lcw_jes_35smcINF_L1J100'] == True)
#             bj_trigger_mask  = (sig['HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30'] == True)
#         elif (mc20 == "mc20d"):
#             LRj_trigger_mask = (sig['HLT_j420_a10t_lcw_jes_40smcINF_L1J100'] == True)
#             bj_trigger_mask  = (sig['HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30'] == True)
#         elif (mc20 == "mc20a"):
#             LRj_trigger_mask = (sig['HLT_j420_a10_lcw_L1J100'] == True) 
#             bj_trigger_mask  = (sig['HLT_j100_2j55_bmv2c2060_split'] == True)
        
        # sig, lumi_weight = apply_trigger(sig, data, trigger)
        # lumi_weight = apply_weights(sig, data)

        if S<170:continue
        truth_X.append(X)
        truth_S.append(S)

        signals[f"{X}_{S}"] = sig
        # lumi_weights[f"{X}_{S}"] = lumi_weight
        # evt_weights[f"{X}_{S}"]  = evt_weight


    # Remove duplicates
    truth_X = list(set(truth_X))
    truth_S = list(set(truth_S))

    # Sort in ascending order
    truth_X.sort()
    truth_S.sort()

    if (debug): print(truth_X)
    if (debug): print(truth_S)
    
    # return signals, lumi_weights, evt_weights, truth_X, truth_S
    return signals, truth_X, truth_S



def get_background(settings):
    
    print('Getting background')
    
    from process_nf_background import process_background
    bkg_average, bkg_up, bkg_dw = process_background(settings)
    
    return bkg_average, bkg_up, bkg_dw
    
    

def get_background_ABCD(data='2018', trigger = "LR", btag = 'Xbb'):
    
    print('Getting ABCD background for', data, 'with trigger', trigger, ' and btag', btag)
    
    if (data == "2016"):
        data_trees = uproot.open('/data/rainbolt/sh4b/fixed_sh4b_selection/background/data16_btag77_xbbtag70.root:trees')
    elif(data == "2017"):
        data_trees = uproot.open('/data/rainbolt/sh4b/fixed_sh4b_selection/background/data17_btag77_xbbtag70.root:trees')
    elif (data == "2018"):
        data_trees = uproot.open('/data/rainbolt/sh4b/fixed_sh4b_selection/background/data18_btag77_xbbtag70.root:trees')
    
    pass_VRL = data_trees['mixed_pass_validation_low'].arrays()
    pass_CRL = data_trees['mixed_pass_control_low'].arrays()
    SR       = data_trees['mixed_pass_signal'].arrays() # This should be blinded
    pass_CRH = data_trees['mixed_pass_control_high'].arrays()
    pass_VRH = data_trees['mixed_pass_validation_high'].arrays()
    
    CR0 = SR

    if (btag == 'Xbb'): 
        fail_VRL = data_trees['mixed_failxbb_validation_low'].arrays()
        fail_CRL = data_trees['mixed_failxbb_control_low'].arrays()
        fail_CR0 = data_trees['mixed_failxbb_signal'].arrays()
        fail_CRH = data_trees['mixed_failxbb_control_high'].arrays()
        fail_VRH = data_trees['mixed_failxbb_validation_high'].arrays()
        CR0 = fail_CR0
        
    elif (btag == '2b'): 
        fail_VRL = data_trees['mixed_fail2b_validation_low'].arrays()
        fail_CRL = data_trees['mixed_fail2b_control_low'].arrays()
        fail_CR0 = data_trees['mixed_fail2b_signal'].arrays()
        fail_CRH = data_trees['mixed_fail2b_control_high'].arrays()
        fail_VRH = data_trees['mixed_fail2b_validation_high'].arrays()
        CR0 = fail_CR0
    else:
        fail_VRL = 0
        fail_CRL = 0
        fail_CR0 = 0
        fail_CRH = 0
        fail_VRH = 0
        
    pass_VRL, _ = apply_trigger(pass_VRL, data, trigger)
    pass_CRL, _ = apply_trigger(pass_CRL, data, trigger)
    pass_CRH, _ = apply_trigger(pass_CRH, data, trigger)
    pass_VRH, _ = apply_trigger(pass_VRH, data, trigger)
    
    if (btag != 'pass'):
        fail_VRL, _ = apply_trigger(fail_VRL, data, trigger)
        fail_CRL, _ = apply_trigger(fail_CRL, data, trigger)
        fail_CR0, _ = apply_trigger(fail_CR0, data, trigger)
        fail_CRH, _ = apply_trigger(fail_CRH, data, trigger)
        fail_VRH, _ = apply_trigger(fail_VRH, data, trigger)
            
    if (btag == 'pass'):
        bkg_total = ak.concatenate([pass_VRL, pass_CRL, pass_CRH, pass_VRH], axis=0)
        TF_total = 1
    else:
        bkg_total = ak.concatenate([fail_VRL,fail_CRL,fail_CR0,fail_CRH,fail_VRH], axis=0)                    
            
    print('bkg_total', bkg_total)
    
    
    if (btag != 'pass'):
        TF_high  = len(pass_CRH)/len(fail_CRH)
        TF_low   = len(pass_CRL)/len(fail_CRL)
        TF_total = (len(pass_CRH) + len(pass_CRL))/(len(fail_CRH) + len(fail_CRL))
        TF_mean  = (TF_high + TF_low)/2
        print ("Transfer factor high  : ", TF_high)
        print ("Transfer factor low   : ", TF_low)
        print ("Transfer factor total : ", TF_total)
        print ("Transfer factor mean  : ", TF_mean)
        print ("Number events         : ", len(fail_CR0))

    
    return CR0, TF_total, bkg_total


def apply_trigger_old(tree, data='2018', trigger = 'OR'):
    
    if (data == "2018"):
        lumi_b = 57.6966 #mc20e (2018)
        lumi_nonb = 58.4501 #mc20e (2018)
        LRj_trigger_mask = (tree['HLT_j420_a10t_lcw_jes_35smcINF_L1J100'] == True)
        bj_trigger_mask  = (tree['HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30'] == True)
    elif (data == "2017"):
        lumi_b = 43.6500 #mc20d (2017)
        lumi_nonb = 44.3074 #mc20d (2017)
        LRj_trigger_mask = (tree['HLT_j420_a10t_lcw_jes_40smcINF_L1J100'] == True)
        bj_trigger_mask  = (tree['HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30'] == True)
    elif (data == "2016"):
        lumi_b = 24.5556  #mc20a (2016)
        lumi_nonb = 32.9881 #mc20a (2016)
        LRj_trigger_mask = (tree['HLT_j420_a10_lcw_L1J100'] == True) 
        bj_trigger_mask  = (tree['HLT_j100_2j55_bmv2c2060_split'] == True)

    #Need to remove events that pass LR jet trigger but have leading LR jet pt < 520 GeV
    LRjet_520_mask = tree["leading_large_r_jet_pt"] > 520
    LRjet_fail520_mask = tree["leading_large_r_jet_pt"] < 520 
    
    mask_LRjets = LRj_trigger_mask& LRjet_520_mask
    mask_LRjets_fail520 = LRj_trigger_mask & LRjet_fail520_mask
    

    lumi_weight = np.where(LRj_trigger_mask, lumi_nonb, lumi_b)
    
    if (trigger == "LR"):
        tree = tree[mask_LRjets]
        lumi_weight = lumi_weight[mask_LRjets]
    elif (trigger == "combined"):
        tree = tree[(mask_LRjets) | bj_trigger_mask]
        lumi_weight = lumi_weight[(mask_LRjets) | bj_trigger_mask]
    elif (trigger == 'OR'):
        tree = tree[~mask_LRjets_fail520]
        lumi_weight = lumi_weight[~mask_LRjets_fail520]
                
    return tree, lumi_weight


def apply_trigger(tree, data='2018', trigger = 'LR'):
    
    if (data == "2018"):
        lumi_b = 58.0339 #mc20e (2018)
        lumi_nonb = 58.7916 #mc20e (2018)
        mask_LRj_trigger = tree['HLT_j420_a10t_lcw_jes_35smcINF_L1J100'] == True
        mask_2b1j        = tree['HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30'] == True
        mask_2b2j        = tree['HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25'] == True
        
    elif (data == "2017"):
        lumi_b = 43.9668 #mc20d (2017)
        lumi_nonb = 44.6306 #mc20d (2017)
        mask_LRj_trigger = tree['HLT_j420_a10t_lcw_jes_40smcINF_L1J100'] == True
        mask_2b1j        = tree['HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30'] == True
        mask_2b2j        = tree['HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25'] == True

    elif (data == "2016"):
        lumi_b = 24.8658 #mc20a (2016)
        lumi_nonb = 33.4022 #mc20a (2016)
        mask_LRj_trigger = tree['HLT_j420_a10_lcw_L1J100'] == True
        mask_2b1j        = tree['HLT_j100_2j55_bmv2c2060_split'] == True
        mask_2b2j        = tree['HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25'] == True

    
    mask_LRjet_520                   = tree["leading_large_r_jet_pt"] > 520  
    mask_3jet_70                     = tree["S_jet1_pt"] > 70

    mask_bucket0                     = mask_LRj_trigger & mask_LRjet_520
    # mask_rejected                    = ~mask_LRj_trigger & mask_LRjet_520
    # mask_resolved                    = ~mask_LRjet_520
    mask_2b1j_noLRcut                = ~mask_LRjet_520 & mask_2b1j
    mask_2b2j_noLRcut                = ~mask_LRjet_520 & mask_2b2j

    # mask_nobucket0_2b1j              = ~mask_bucket0 & mask_2b1j
    # mask_nobucket0_2b2j              = ~mask_bucket0 & mask_2b2j

    mask_bucket0_2b1j                = mask_bucket0 | mask_2b1j_noLRcut #less inclusive than mask_nobucket0_2b1j (does not include rejected)
    mask_bucket0_2b2j                = mask_bucket0 | mask_2b2j_noLRcut #less inclusive than mask_nobucket0_2b1j (does not include rejected)

    mask_bucket1                     = ~mask_LRj_trigger & mask_3jet_70 & mask_2b1j
    mask_bucket2                     = ~mask_LRj_trigger & ~mask_3jet_70 & mask_2b2j

    mask_bucket012                   = mask_bucket0 | mask_bucket1 | mask_bucket2
    
    #assign wights according to the luminosity
    lumi_weight = np.where(mask_LRj_trigger, lumi_nonb, lumi_b)
    
    if (trigger == "LR"):
        tree = tree[mask_bucket0]
        lumi_weight = lumi_weight[mask_bucket0]
    elif (trigger == "LR_2b1j"):
        tree = tree[mask_bucket0_2b1j]
        lumi_weight = lumi_weight[mask_bucket0_2b1j]
    elif (trigger == "LR_2b2j"):
        tree = tree[mask_bucket0_2b2j]
        lumi_weight = lumi_weight[mask_bucket0_2b2j]
    elif (trigger == 'LR_resolved'):
        tree = tree[mask_bucket012]
        lumi_weight = lumi_weight[mask_bucket012]
                
    return tree, lumi_weight
   
    
def apply_weights(sig, data='2018'):
    
    if (data == "2018"):
        lumi_b = 58.0339 #mc20e (2018)
        lumi_nonb = 58.7916 #mc20e (2018)
        
    elif (data == "2017"):
        lumi_b = 43.9668 #mc20d (2017)
        lumi_nonb = 44.6306 #mc20d (2017)
        
    elif (data == "2016"):
        lumi_b = 24.8658 #mc20a (2016)
        lumi_nonb = 33.4022 #mc20a (2016)
        
    lumi_weight = {
        '2b1j': np.array([lumi_b] * len(sig['2b1j'])),
        'LR': np.array([lumi_nonb] * len(sig['LR']))
    }
        
    return lumi_weight 
    
#     #assign wights according to the luminosity
#     lumi_weight = np.where(mask_LRj_trigger, lumi_nonb, lumi_b)
    
#     if (trigger == "LR"):
#         tree = tree[mask_bucket0]
#         lumi_weight = lumi_weight[mask_bucket0]
#     elif (trigger == "LR_2b1j"):
#         tree = tree[mask_bucket0_2b1j]
#         lumi_weight = lumi_weight[mask_bucket0_2b1j]
#     elif (trigger == "LR_2b2j"):
#         tree = tree[mask_bucket0_2b2j]
#         lumi_weight = lumi_weight[mask_bucket0_2b2j]
#     elif (trigger == 'LR_resolved'):
#         tree = tree[mask_bucket012]
#         lumi_weight = lumi_weight[mask_bucket012]


s_mass_bounds = \
{
    70: (51.016492966332834, 86.10361152805339),
    100: (41.93689704145831, 144.35017581978818),
    170: (117.44606353123945, 207.54474446805628),
    200: (136.59141836353228, 237.00081501252282),
    250: (168.73831889142787, 291.65261571049314),
    300: (201.99719986383417, 348.2991825678708),
    400: (267.6219944794021, 458.1254853548739),
    500: (336.08535875740887, 565.1127691201983),
    750: (509.4051967753908, 830.3135969236234),
    1000: (683.0826778437853, 1101.1336318837016),
    1500: (1006.9508899816418, 1639.7853840351195),
    2000: (1308.886662166531, 2200.1970730120374),
    2500: (1712.0512286740889, 2697.4648775331734),
    3000: (1989.1118038710538, 3270.6058611941608),
    4000: (2615.6022694276903, 4359.170135161537),
    5000: (3461.3982048188245, 5434.148975902206)
}

def s_mass_cut_string(nominal_s_mass, ms):
    min_s_mass = s_mass_bounds[nominal_s_mass][0]
    max_s_mass = s_mass_bounds[nominal_s_mass][1]
    mask = (ms > min_s_mass) & (ms < max_s_mass)
    return mask
    # return "(S_mass > {:.3f}) & (S_mass < {:.3f})".format(min_s_mass, max_s_mass)


def signal_region(mh, ms, S):
    
    return np.sqrt(((mh - 125) / (0.1 * mh))**2 + ((ms - S) / (0.1 * ms))**2)
    
    
# def make_signal_histograms(signals, lumi_weights, evt_weights, sigma, sig_region=None, xbins=None):
def make_signal_histograms(signals, sigma, sig_region=None, xbins=None):
    '''
    
    Args:
        sig_region (str): The signal region defition (options: None, 1dfit, 2dfit, xsh)
    '''
    
    if xbins is None:
        xbins = _default_xbins_

    sig_hists = {}
    
    for key, signal in signals.items():
        
        X = key.split('_')[0]
        S = key.split('_')[1]
        
        X = float(X)
        S = float(S)

        mh = signal['H_mass']
        ms = signal['S_mass']
        mx = signal['X_mass']

        if sig_region == '1dfit': 
            mask_sr = s_mass_cut_string(S, ms)
            sig = signal[mask_sr]
            # lumi_wgt = lumi_weights[key][mask_sr]
        elif sig_region == '2dfit':
            _, _, mask_sr = apply_2d_cut(X, S, mx, ms)
            sig = signal[mask_sr]
            # lumi_wgt = lumi_weights[key][mask_sr]
        elif sig_region == 'xsh':
            mask_sr = signal_region(mh, ms, S) < 1.6
            sig = signal[mask_sr]
            # lumi_wgt = lumi_weights[key][mask_sr]
        else: 
            sig = signal
            # lumi_wgt = lumi_weights[key]
            
        # sig_weights = (lumi_wgt * sigma)/evt_weights[key]
        sig_weights = (sig['lumi_weight'] * sigma) / sig['sum_of_weights']
        sig_weights = sig_weights * sig['pileup_weight']
        sig_weights = sig_weights * sig['beamspot_weight']
        # sig_hists[key] = np.histogram(ak.to_numpy(sig["X_mass"]), bins=xbins, weights=ak.to_numpy(sig_weights))

        values = ak.to_numpy(sig["X_mass"])
        weights = ak.to_numpy(sig_weights)
        
        h, bins = np.histogram(values, bins=xbins, weights=weights)
        h_w2, _ = np.histogram(values, bins=xbins, weights=weights**2)

        histogram = (
            h,            # the bin content
            bins,         # the bins
            np.sqrt(h_w2) # the uncertainty per bin
        )

        sig_hists[key] = histogram
        
    return sig_hists


def make_background_histograms(background, transfer_factor, mass_points, X_SH=True, xbins=None):
    
    if xbins is None:
        xbins = _default_xbins_

    bkg_hists = {}
    
    for key in mass_points:
        
        X = key.split('_')[0]
        S = key.split('_')[1]
        
        X = float(X)
        S = float(S)
        
        mh = background['H_mass']
        ms = background['S_mass']
        if X_SH: 
            mask_sr = s_mass_cut_string(ms)
            # mask_sr = signal_region(mh, ms, S) < 1.6
            bkg = background[mask_sr]
        else:
            bkg = background
            
        bkg_weights = [transfer_factor] * len(bkg["X_mass"])
        bkg_hists[key] = np.histogram(bkg["X_mass"], bins=xbins, weights=np.array(bkg_weights))
        
    return bkg_hists
        

def make_histograms(signals, lumi_weights, evt_weights, sigma,
                    background, transfer_factor, sig_region='2dfit', xbins=None):
    '''
    
    Args:
        sig_region (str): The signal region defition (options: None, 1dfit, 2dfit)
    '''
    
    sig_hists = make_signal_histograms(signals, lumi_weights, evt_weights, sigma, sig_region, xbins)
    bkg_hists = make_background_histograms(background, transfer_factor, list(signals.keys()), sig_region, xbins)
    
    return sig_hists, bkg_hists


def sum_histograms_by_year(hists_1, hists_2, hists_3):
    
    hists_all = {}
        
    for key in hists_1:
        hists_all[key] = sum_histograms(hists_1[key], hists_2[key])
        hists_all[key] = sum_histograms(hists_all[key], hists_3[key])
        # hist_value = hists_1[key][0] + hists_2[key][0] + hists_3[key][0]
        # hist_value is an Awkward array, convert it to numpy
        # hist_value = ak.to_numpy(hist_value)
        # hists_all[key] = [hist_value, hists_1[key][1]]
                  
    return hists_all

def sum_histograms(hist1, hist2):
    h = hist1[0] + hist2[0]
    b = hist1[2]

    err1 = hist1[2]
    err2 = hist2[2]
    err = np.sqrt(err1**2 + err2**2)

    return (h, b, err)


def finite_divide(numerator, denominator):
    if denominator == 0:
        return 0
    else:
        return numerator / denominator

    
def create_variation_histograms(hists_1, hists_2, outdir='./plots_variation_histo_', year='2018', name='Xbb'):
    
    if not os.path.exists(outdir+year):
        os.makedirs(outdir+year)
        
    print(f'Plots will be saved in {outdir+year}')

    xbins = _default_xbins_
    n_bins = len(xbins) - 1
    
    ratio_histos = {}
    #Normalize
    for key in hists_1:
        integral_1 = np.sum(hists_1[key][0])
        bin_counts_1 = [finite_divide(hists_1[key][0][i], integral_1) for i in range(n_bins)]
        integral_2 = np.sum(hists_2[key][0])
        bin_counts_2 = [finite_divide(hists_2[key][0][i], integral_2) for i in range(n_bins)]
        
        ratio = [finite_divide(bin_counts_1[i], bin_counts_2[i]) for i in range(n_bins)]
        integral = np.sum(ratio)
        
        # for i in range(n_bins):
        #     print("i", i)
        #     print("ratio", ratio[i])
        #     print("integral", integral)
            
        ratio_histos[key] = [finite_divide(ratio[i], integral) for i in range(n_bins)]
        # ratio_histos[key][1] = xbins
        
        
        fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True,  figsize=(5, 5),
                               gridspec_kw={'height_ratios': [3, 1]})
        
        ax[0].stairs(bin_counts_1, xbins, label=name, color=_colors_[0])
        ax[0].stairs(bin_counts_2, xbins, label='Pass', color=_colors_[1])

        ax[1].stairs(ratio, xbins, label='Ratio', color=_colors_[1])
        ax[1].axhline(1.1, color='gray')
        ax[1].axhline(0.9, color='gray')
        
        X = key.split('_')[0]
        S = key.split('_')[1]
        
        ax[0].legend()
        ax[0].set_ylabel('Events')
        ax[0].set_title(f'X {X}, S {S}')
        ax[1].set_xlabel('X mass [GeV]')
        ax[1].set_ylabel('Ratio')
        ax[1].set_ylim([0.5, 1.5])
        plt.savefig(outdir+year+ f'/hists_X{X}_S{S}_'+name+'.pdf')
        # plt.show()
        plt.close()
        

    return ratio_histos


def plot_histograms(sig_hists, bkg_hists, ratio_up=None, ratio_dw=None, outdir='./plots_histo_', year='2018'):
    
    if not os.path.exists(outdir+year):
        os.makedirs(outdir+year)
        
    print(f'Plots will be saved in {outdir+year}')
    
    # var_up_hists, var_down_hists, uncs = create_variation_histograms(bkg_hists, 'poisson', 10000)# default_n_iterations)

    for key, signal in sig_hists.items():
        
        X = key.split('_')[0]
        S = key.split('_')[1]
                
        # if (key =="6000_3000"): continue 
        # if (key =="750_250"): continue 
        # if (key =="750_200"): continue 
        
              
        s_hist = signal[0]
        b_hist = bkg_hists[key][0]
        
        if ratio_up is not None and ratio_dw is not None:
            b_var_up_hist = (ratio_up[key] * b_hist) + b_hist
            b_var_dw_hist = (ratio_dw[key] * b_hist) + b_hist
        
        bins = signal[1]
    
        fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(5, 5))
        
        # if bkg_var_hists is not None:
        #     plt.stairs(b_var_hist, bins, label='Background Var', color=_colors_[0], linestyle='dashed')
        
        plt.stairs(b_hist, bins, label='Background', color=_colors_[0])
        plt.stairs(s_hist, bins, label='Signal', color=_colors_[2])
        
        if ratio_up is not None and ratio_dw is not None:
            plt.stairs(b_var_up_hist, bins, label='Up Variation', color=_colors_[3], linestyle='dashed', alpha=0.8)
            plt.stairs(b_var_dw_hist, bins, label='Down Variation', color=_colors_[5], linestyle='dotted', alpha=0.8)

        
        bin_centers = 0.5 * (bins[:-1] + bins[1:])
    
        
#         # print('var_up_hists',var_up_hists[key][0])
#         # print('b_hist', b_hist)
#         # print('var_down_hists',var_down_hists[key][0])
#         # print('****************************')
#         yerr_up   = var_up_hists[key][0] - b_hist
#         noneg_yerr_up = np.maximum(yerr_up, 0)
#         yerr_down = b_hist - var_down_hists[key][0]
#         noneg_yerr_down = np.maximum(yerr_down, 0)
#         # print('noneg_yerr_up',noneg_yerr_up)
#         # print('b_hist', b_hist)
#         # print('noneg_yerr_down',noneg_yerr_down)
        
#         # print(var_down_hists[key][0])
#         # plt.errorbar(bin_centers, b_hist, yerr=[noneg_yerr_down, noneg_yerr_up], fmt='none')
#         plt.errorbar(bin_centers, b_hist, yerr=uncs[key], fmt='none')
#         plt.errorbar(bin_centers, s_hist, yerr=np.sqrt(s_hist), fmt='none')
#         # plt.plot(bins[:-1]+0.5, data, 'o', color='black', markersize=3, label='Observation')

        ax.legend()
        ax.set_xlabel('X mass [GeV]')
        ax.set_ylabel('Events')
        ax.set_title(f'X {X}, S {S}')
        plt.savefig(outdir+year + f'/hists_X{X}_S{S}.pdf')
        # plt.show()
        plt.close()
        
    
# def get_2d_signal_hists(trig='LR_2b1j', X_SH=False):
    
#     sigma = 1
    
#     sig_weights = (lumi_weights_2018 * sigma)/evt_weights_2018[key]
    
#     signals_2018, lumi_weights_2018, evt_weights_2018, truth_X, truth_S = get_signal(data="2018", mc20="mc20e", trigger = trig)
    

def prepare_hist_all(settings=None, include_bkg=True):
    '''
    
    Args:
        trig (str): The trigger type
        sig_region (str): The signal region defition (options: None, 1dfit, 2dfit)
    '''
    
    if settings is None:
        settings = {
            'directory': '/data/rainbolt/sh4b/mixed_selection/background/',
            'n_seeds': 3,
            'trigger_tag': 'bucketLR2b1j',
            'model_tag': "",
            'sig_region': '2dfit',
            'bins': None,
            'info': '',
        }

    if settings['bins'] is None:
        settings['bins'] = _default_xbins_

    signals_2018, truth_X, truth_S = get_signal(data="2018", mc20="mc20e", btag=settings['btag'], xbbtag=settings['xbbtag'])
    sig_hists_2018 = make_signal_histograms(signals_2018, sigma=1, sig_region=settings['sig_region'], xbins=settings['bins'])
    
    signals_2017, truth_X, truth_S = get_signal(data="2017", mc20="mc20d", btag=settings['btag'], xbbtag=settings['xbbtag'])
    sig_hists_2017 = make_signal_histograms(signals_2017, sigma=1, sig_region=settings['sig_region'], xbins=settings['bins'])
    
    signals_2016, truth_X, truth_S = get_signal(data="2016", mc20="mc20a", btag=settings['btag'], xbbtag=settings['xbbtag'])
    sig_hists_2016 = make_signal_histograms(signals_2016, sigma=1, sig_region=settings['sig_region'], xbins=settings['bins'])
    
    sig_hists = sum_histograms_by_year(sig_hists_2018, sig_hists_2017, sig_hists_2016)
    
    if include_bkg:
        bkg_hists, bkg_hists_up, bkg_hists_dw = get_background(settings)

        return truth_X, truth_S, sig_hists, bkg_hists, bkg_hists_up, bkg_hists_dw
    
    else:
        return truth_X, truth_S, sig_hists



