'''
sig_region_2d_fit
'''
import os
import numpy as np
import awkward as ak
from scipy.stats import chi2
import pickle

def gaussian_2d(xy, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    x, y = xy
    xo = float(xo)
    yo = float(yo)
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude * np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) + c*((y-yo)**2)))
    return g.ravel()


def apply_2d_cut(x_t, s_t, x, s):
    '''
    
    Args:
        x_t (float): The truth value of X
        s_t (float): The truth value of S
        x (list): The values of the X masses
        s (list): The values of the S masses
        
    Returns:
        list: The values of the X masses inside the contour
        list: The values of the S masses inside the contour
        list: The mask
    '''
    x = ak.to_numpy(x)
    s = ak.to_numpy(s)

    file_name = f'XS_2d_fit_results/X{x_t:.0f}_S{s_t:.0f}.pkl'

    if os.path.exists(file_name):
        with open(file_name, 'rb') as f:
            data = pickle.load(f)
    else:
        return x, s, [True] * len(x)

    # gaussian_2d = data['function']
    fitted_params = data['params']

    # result = gaussian_2d((2800, 800), *fitted_params)


    # Compute the 99% contour threshold
    sigma_level_99 = chi2.ppf(0.99, df=2)
    contour_threshold_99 = fitted_params[0] * np.exp(-sigma_level_99 / 2) + fitted_params[6]

    # Select points inside the 99% contour
    # x = np.array([2800, 3800])
    # s = np.array([800, 600])
    gaussian_values = gaussian_2d((x, s), *fitted_params)
    inside_99_contour = gaussian_values >= contour_threshold_99

    # Extract only the points inside the 99% contour
    x_inside = x[inside_99_contour]
    s_inside = s[inside_99_contour]

    return x_inside, s_inside, inside_99_contour


if __name__ == '__main__':
    key = '3000_1000'
    X = float(key.split('_')[0])
    S = float(key.split('_')[1])
    
    x = np.array([2800, 3800])
    s = np.array([800, 600])
    
    apply_2d_cut(X, S, x, s)